using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class Initialization : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        var defaultHand = GameObject.Find("RightHand");

        GameObject.Find("Drawing").transform.SetParent(defaultHand.transform);

        GameObject.Find("Pallette").transform.SetParent(defaultHand.GetComponent<Hand>().otherHand.transform);
        GameObject.Find("Pallette").transform.position = defaultHand.GetComponent<Hand>().otherHand.transform.position;
        GameObject.Find("Pallette").transform.position += new Vector3(-.11f, 0f, -.146f);
        GameObject.Find("Pallette").transform.rotation = Quaternion.Euler(0f, 90f, 200f);
    }

    // Update is called once per frame
    void Update() { }
}

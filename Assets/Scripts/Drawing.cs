using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class Drawing : MonoBehaviour
{
    public SteamVR_Action_Boolean drawAction = SteamVR_Input.GetBooleanAction("Draw");
    public GameObject editor;
    public GameObject boxTemplate;

    private Vector3 start;
    private GameObject currentDrawing;
    private bool drawBox = true;
    private GameObject pointer;

    // Start is called before the first frame update
    void Start()
    {
        // pointer = currentDrawing = Instantiate(boxTemplate, editor.transform);
        // pointer.transform.localScale = new Vector3(.01f, .01f, .01f);
        // Destroy(pointer.GetComponent<Throwable>());
        // Destroy(pointer.GetComponent<Interactable>());
        // pointer.GetComponent<BoxCollider>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        var hand = transform.parent.gameObject.GetComponent<Hand>(); // TODO: get this in Start()
        // if (hand.skeleton.indexCurl < .1)
        //     pointer.transform.position = hand.skeleton.indexTip.position;
        if (drawAction.GetStateDown(hand.handType))
        {
            start = hand.transform.position;
            currentDrawing = Instantiate(boxTemplate, editor.transform);
            currentDrawing.GetComponent<Interactable>().enabled = false; // TODO: Figure out how to disable hover events from the hand
            if (drawBox)
            {
                currentDrawing.transform.localScale = new Vector3(0, 0, 0);
            }
            currentDrawing.transform.position = start;
        }
        else if (drawBox && drawAction.GetState(hand.handType))
        {
            var end = hand.transform.position;
            currentDrawing.transform.localScale = end - start;
            currentDrawing.transform.position = (start + end) / 2;
        }
        else if (drawAction.GetStateUp(hand.handType))
        {
            currentDrawing.GetComponent<Interactable>().enabled = true;
        }
    }

    public void loopClick()
    {
        Debug.Log("In loopClick");
    }
    public void addClick()
    {
        Debug.Log("In addClick");
    }
    public void funcClick()
    {
        Debug.Log("In funcClick");
    }
    public void wireClick()
    {
        Debug.Log("In wireClick");
    }
}

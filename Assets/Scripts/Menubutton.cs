using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System;
using Valve.VR.InteractionSystem;

//-------------------------------------------------------------------------
[RequireComponent(typeof(Interactable))]
public class Menubutton : MonoBehaviour
{
    public CustomEvents.UnityEventHand onHandClick;

    protected Hand currentHand;

    protected virtual void Awake()
    {
        Button button = GetComponent<Button>();
        if (button)
        {
            button.onClick.AddListener(OnButtonClick);
        }
    }

    protected virtual void OnHandHoverBegin(Hand hand)
    {
        currentHand = hand;
        InputModule.instance.Submit( gameObject );
    }

    protected virtual void OnButtonClick()
    {
        onHandClick.Invoke(currentHand);
    }
}

#if UNITY_EDITOR
[UnityEditor.CustomEditor(typeof(Menubutton))]
public class UIElementEditor : UnityEditor.Editor
{
    // Custom Inspector GUI allows us to click from within the UI
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Menubutton uiElement = (Menubutton)target;
        if (GUILayout.Button("Click"))
        {
            InputModule.instance.Submit(uiElement.gameObject);
        }
    }
}
#endif

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PalleteHandler : MonoBehaviour
{
    public GameObject[] buttons;

    public void Click(GameObject source)
    {
        for (int i = 0; i < buttons.Length; i++)
        {
            var txt = buttons[i].gameObject.GetComponentInChildren<Text>();
            if (buttons[i] == source)
            {
                txt.text = "<b><color=blue>" + buttons[i].name + "</color></b>";
            }
            else
            {
                txt.text = buttons[i].name;
            }
        }
    }
}
